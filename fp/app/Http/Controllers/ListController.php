<?php

namespace App\Http\Controllers;

class ListController extends Controller
{
    public function show()
    {
       $tiket = [
         'Surabaya - Jakarta'    => 'Loket 1',
         'Surabaya - Yogyakarta' => 'Loket 2',
         'Surabaya - Magelang'   => 'Loket 3',
         'Surabaya - Semarang'   => 'Loket 4',
         'Surabaya - Madura'     => 'Loket 5',
         'Surabaya - Banyuwangi' => 'Loket 6',
         'Surabaya - Bali'       => 'Loket 7'

       ];

       return view('welcome')->withTiket($tiket);
    }
}
