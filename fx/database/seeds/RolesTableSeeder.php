<?php

use Illuminate\Database\Seeder;
//use Illuminate\Database\Eloquent\Model;
use App\Role as Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Role::insert([
            ['name'=>'Admin', 'description'=>'Admin'],
            ['name'=>'User', 'description'=>'User',],
            ['name'=>'Guest', 'description'=>'Tamu']
        ]);
    }
}
