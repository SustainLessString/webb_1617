<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetilBus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detilBus', function (Blueprint $table) {
            // detilBus
            $table->string('id', 4)->primary();
            $table->string('nama_bus', 20);
            $table->string('kelas', 2)
                  ->references('id')->on('kelasBus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detilBus');
    }
}
