<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRuteBus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruteBus', function (Blueprint $table) {
            // Rute
            $table->time('waktu_berangkat');
            $table->time('waktu_tiba');
            $table->char('berangkat', 4)
                  ->references('id')->on('kodeArea')
                  ->onUpdate('cascade');
            $table->char('tiba', 4)
                  ->references('id')->on('kodeArea')
                  ->onUpdate('cascade');
            $table->string('bus', 4)
                  ->references('id')->on('detilBus')
                  ->onUpdate('cascade');
            $table->primary(array('waktu_berangkat', 'waktu_tiba', 'berangkat', 'tiba', 'bus'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruteBus');
    }
}
