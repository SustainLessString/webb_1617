<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruteBus extends Model
{
    // Rute
    public $fillable = [
        'id',
        'waktu_berangkat',
        'waktu_tiba',
        'berangkat',
        'tiba',
        'bus'
    ];
}
