<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detilBus extends Model
{
    // Detil Bus
    public $fillable = [
        'id',
        'nama_bus',
        'kelas'
    ];
}
