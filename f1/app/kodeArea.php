<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kodeArea extends Model
{
    // Kode Area
    public $fillable = [
        'id',
        'nama_area'
    ];
}
