<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelasBus extends Model
{
    // Kelas Bus
    public $fillable = [
        'id',
        'nama_kelas'
    ];
}
